/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot;

import java.awt.image.BufferedImage;

/**
 * Hello world!
 *
 */

public interface Printable {
	public BufferedImage getImage();
}
