package org.sikuli.makerbot.solid;

import java.awt.image.BufferedImage;


public class SolidFactory {

	public static Solid createLotusRootSetSolidModel(BufferedImage input){
		return LotusRootSetSolid.createFrom(input);
	}
}
