/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot.solid;

import org.stringtemplate.v4.STGroupFile;

public abstract class AbstractSolid {	
	abstract public String toSCAD();
	static STGroupFile g = new STGroupFile("org/sikuli/makerbot/solid/scad.stg", "utf-8", '$', '$');
}
