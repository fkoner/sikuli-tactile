/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot.solid;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.stringtemplate.v4.ST;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.*;

public class LotusRoot extends AbstractSolid {		
	Contour boundary;
	private List<Contour> holes = new ArrayList<Contour>();


	public LotusRoot(Contour boundaryContour) {
		this.boundary = boundaryContour;
	}

	void paintContourAsSolidPolygon(CvSeq contour, IplImage image, CvScalar color){		
		CvMemStorage storage = CvMemStorage.create();
		CvSeq polygon_points = cvApproxPoly(contour, Loader.sizeof(CvContour.class),
				storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.002, 0);
		CvPoint cvPoints = new CvPoint(polygon_points.total());
		cvCvtSeqToArray(polygon_points, cvPoints, CV_WHOLE_SEQ);
		int[] npts = {polygon_points.total()};
		cvFillPoly(image, cvPoints, npts, 1, color, CV_AA, 0);
	}
	
	void paintContourAsSolid(CvSeq contour, IplImage image, CvScalar color){		
//		CvMemStorage storage = CvMemStorage.create();
//		CvSeq polygon_points = cvApproxPoly(contour, Loader.sizeof(CvContour.class),
//				storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.002, 0);
//		CvPoint cvPoints = new CvPoint(polygon_points.total());
//		cvCvtSeqToArray(polygon_points, cvPoints, CV_WHOLE_SEQ);
//		int[] npts = {polygon_points.total()};
//		cvFillPoly(image, cvPoints, npts, 1, color, CV_AA, 0);
		cvDrawContours(image, contour, color, color, 1, CV_FILLED, 4);
	}


	public void paintOn(IplImage image){			
		//paintContourAsSolidPolygon(boundary.getCvSeq(), image, CvScalar.BLUE);
		paintContourAsSolid(boundary.getCvSeq(), image, CvScalar.BLUE);
		for (Contour hole : getHoles()){			
			//paintContourAsSolidPolygon(hole.getCvSeq(), image, CvScalar.BLACK);
			paintContourAsSolid(hole.getCvSeq(), image, CvScalar.BLACK);
		}
	}

	static List<Integer> generateIndices(int n){
		List<Integer> indices = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {	
			indices.add(i);
		}
		return indices;
	}	


	//static STGroupFile g = new STGroupFile("templates/scad.stg", "utf-8", '$', '$');
	public String toSCAD(){

		List<ST> holeSTs = new ArrayList<ST>();
		for (Contour hole : getHoles()){			
			ST st = g.getInstanceOf("hole");
			List<Point> points = hole.toPoints();
			st.add("points", points);
			st.add("indices", generateIndices(points.size()));
			holeSTs.add(st);
		}

		ST boundaryST = g.getInstanceOf("boundary");
		List<Point> points = boundary.toPoints();
		boundaryST.add("points", points);
		boundaryST.add("indices", generateIndices(points.size()));

		ST st = g.getInstanceOf("lotusroot");		
		st.add("boundary", boundaryST);
		st.add("holes", holeSTs);

		return st.render();
	}

	public List<Contour> getHoles() {
		return holes;
	}
}
