package org.sikuli.makerbot.solid;

public interface Solid {
	abstract public String toSCAD();
}
